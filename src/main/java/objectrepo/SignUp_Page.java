package objectrepo;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SignUp_Page
{
    AndroidDriver driver;

    public SignUp_Page(AndroidDriver driver)
    {
        this.driver= driver;
    }

    By mobile1 = By.id("com.livquik.future.fpdroid:id/et_mobile_walkthrough_activity");
    By btn = By.id("com.livquik.future.fpdroid:id/ibtn_mobile_walkthrough_activity");
    By fname = By.id("com.livquik.future.fpdroid:id/et_first_name_fragment_sign_up");
    By lname = By.id("com.livquik.future.fpdroid:id/et_last_name_fragment_sign_up");
    By email = By.id("com.livquik.future.fpdroid:id/et_email_fragment_sign_up");
    By dob = By.id("com.livquik.future.fpdroid:id/et_dob_fragment_sign_up");
    By okbtn = By.id("android:id/button1");
    By signupbtn = By.id("com.livquik.future.fpdroid:id/btn_sign_up_fragment_sign_up");

    public WebElement mbl()
    {
        return driver.findElement(mobile1);
    }

    public WebElement btn()

    {
        return driver.findElement(btn);
    }
    public WebElement fname()

    {
        return driver.findElement(fname);
    }
    public WebElement lname()

    {
        return driver.findElement(lname);
    }
    public WebElement email()

    {
        return driver.findElement(email);
    }
    public WebElement dob()

    {
        return driver.findElement(dob);
    }
    public WebElement okbtn()

    {
        return driver.findElement(okbtn);
    }
    public WebElement signupbtn()

    {
        return driver.findElement(signupbtn);
    }
}
