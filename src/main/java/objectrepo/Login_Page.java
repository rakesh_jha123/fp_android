package objectrepo;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class Login_Page
{
    AndroidDriver driver;

    public Login_Page(AndroidDriver driver)
    {
        this.driver= driver;
    }

    By mobile = By.id("com.livquik.future.fpdroid:id/et_mobile_walkthrough_activity");
    By btn = By.id("com.livquik.future.fpdroid:id/ibtn_mobile_walkthrough_activity");

    public WebElement mbl()
    {
        return driver.findElement(mobile);
    }

    public WebElement btn()

    {
        return driver.findElement(btn);
    }


}
