package TestCases;

import io.appium.java_client.android.AndroidDriver;
import objectrepo.Login_Page;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class login
{
    AndroidDriver driver;


    @BeforeClass
    @Parameters({"BROWSER_NAME","VERSION","deviceName","platformName","appPackage","appActivity"})
    public void setUp(String BROWSER_NAME, String VERSION, String deviceName, String platformName, String appPackage, String appActivity) throws MalformedURLException
    {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("BROWSER_NAME", BROWSER_NAME);
        capabilities.setCapability("VERSION", VERSION);
        capabilities.setCapability("deviceName",deviceName);
        capabilities.setCapability("platformName",platformName);
        capabilities.setCapability("appPackage", appPackage);
        capabilities.setCapability("appActivity",appActivity);
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("unicodeKeyboard", false);
        capabilities.setCapability("resetKeyboard", false);
        capabilities.setCapability("autoGrantPermissions", true);



        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        System.out.println("app startedd");

    }
    @Parameters({"mobile"})
    @Test
    public void login(String mobile) throws Exception
    {
        Login_Page l = new Login_Page(driver);
        l.mbl().sendKeys(mobile);
        l.btn().click();

        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp1")).click();
        driver.getKeyboard().sendKeys("9");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp2")).click();
        driver.getKeyboard().sendKeys("8");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp3")).click();
        driver.getKeyboard().sendKeys("1");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp4")).click();
        driver.getKeyboard().sendKeys("9");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp5")).click();
        driver.getKeyboard().sendKeys("6");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp6")).click();
        driver.getKeyboard().sendKeys("3");

        System.out.println("login successful");

    }
    public void teardown(){
        //close the app
        driver.quit();
    }
}
