package TestCases;

import io.appium.java_client.android.AndroidDriver;
import objectrepo.SignUp_Page;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by livquik-tech on 2/5/19.
 */
public class SignUp
{
    AndroidDriver driver;


    @BeforeClass
    @Parameters({"BROWSER_NAME","VERSION","deviceName","platformName","appPackage","appActivity"})
    public void setUp(String BROWSER_NAME, String VERSION, String deviceName, String platformName, String appPackage, String appActivity) throws MalformedURLException
    {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("BROWSER_NAME", BROWSER_NAME);
        capabilities.setCapability("VERSION", VERSION);
        capabilities.setCapability("deviceName",deviceName);
        capabilities.setCapability("platformName",platformName);
        capabilities.setCapability("appPackage", appPackage);
        capabilities.setCapability("appActivity",appActivity);
        capabilities.setCapability("automationName", "UiAutomator2");
        capabilities.setCapability("unicodeKeyboard", false);
        capabilities.setCapability("resetKeyboard", false);
        capabilities.setCapability("autoGrantPermissions", true);


        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
       /* driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);*/

        System.out.println("app started");

    }
    @Parameters({"mobile1", "fname", "lname", "email"})
    @Test
    public void login(String mobile1, String fname, String lname, String email) throws Exception
    {
        SignUp_Page rg = new SignUp_Page(driver);
        rg.mbl().sendKeys(mobile1);
        rg.btn().click();
        rg.fname().sendKeys(fname);
        rg.lname().sendKeys(lname);
        rg.email().sendKeys(email);
        rg.dob().click();
        rg.okbtn().click();
        rg.signupbtn().click();

        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp1")).click();
        driver.getKeyboard().sendKeys("9");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp2")).click();
        driver.getKeyboard().sendKeys("8");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp3")).click();
        driver.getKeyboard().sendKeys("1");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp4")).click();
        driver.getKeyboard().sendKeys("9");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp5")).click();
        driver.getKeyboard().sendKeys("6");
        driver.findElement(By.id("com.livquik.future.fpdroid:id/otp6")).click();
        driver.getKeyboard().sendKeys("3");

        System.out.println("signup successful");

    }
    public void teardown(){
        //close the app
        driver.quit();
    }
}
