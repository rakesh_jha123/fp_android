package screenshot;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;

/**
 * Created by livquik-tech on 13/6/19.
 */
public class takescreenshot {
    public static String CaptureScreenshot(WebDriver driver, String screenshotname)
    {
        try {

            TakesScreenshot ts=(TakesScreenshot)driver;
            File source=ts.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(source, new File("./screenshot/"+screenshotname+".png"));
            System.out.println("screenshot taken");
        }
        catch (Exception e)
        {
            System.out.println("exception while taking screenshot"+e.getMessage());
        }
        return screenshotname;

    }

}
