package AdvanceReport;

import Library.Utility;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
/**
 * Created by livquik-tech on 18/7/18.
 */
public class ExtentReport
{
    //create global veriable
    ExtentReports report;
    ExtentTest logger;
    WebDriver driver;

    @BeforeSuite
    public void title ()
    {
        //using extent report create object of this class
        report = new ExtentReports("//home//livquik-tech//Desktop//screenshot//Report//REPORTS.html");
        logger = report.startTest("verify title");

    }
    @AfterSuite
    public void tearDown(ITestResult result)
    {
        if(result.getStatus()==ITestResult.FAILURE)
        {
            String screenshot_path = Utility.CaptureScreenshot(driver,result.getName());
            //   String image= logger.addScreenCapture(screenshot_path);
            //  logger.log(LogStatus.FAIL, "Title verification", image);
        }

    }

}
