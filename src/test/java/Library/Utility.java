package Library;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import java.io.File;

/**
 * Created by livquik-tech on 30/4/19.
 */
public class Utility
{

    @Test
    public static String CaptureScreenshot (WebDriver driver, String screenshotname)
    {
        try {
            TakesScreenshot ts=(TakesScreenshot)driver;
            File source=ts.getScreenshotAs(OutputType.FILE);
            String des = "//home//livquik-tech//Desktop//screenshot//"+screenshotname+".png";
            File destination = new File (des);
            FileUtils.copyFile(source, destination);
            System.out.println("screenshot taken");
            return des;

        }
        catch (Exception e)
        {
            System.out.println("exception while taking screenshot "+e.getMessage());
            return e.getMessage();
        }
    }

}
