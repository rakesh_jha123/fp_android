package Assertion;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Assert
{
    SoftAssert softAssert = new SoftAssert();
    String className = "User Loged In Successfully";

    @Test
    public void test_UsingSoftAssertion()
    {
        softAssert.assertTrue(true == true);
        softAssert.assertEquals("success", "User Loged In Successfully");
        softAssert.assertEquals(className, "User Loged In Successfully");
        System.out.println("Last statement gets executed!");
        softAssert.assertAll();
    }
}
