package Listerner;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNG_Listerner implements ITestListener
{

    @Override
    public void onTestStart(ITestResult result)
    {
        System.out.println("Details: test case start and testcases details are " + result.getName());

    }

    @Override
    public void onTestSuccess(ITestResult result)
    {
        System.out.println("Details: test case success and testcases details are " + result.getName());
        System.out.println("Details: test case execute successfully " + result.getName());
    }

    @Override
    public void onTestFailure(ITestResult result)
    {
        System.out.println("details: test case failed and details are " + result.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result)
    {
        System.out.println("Details: test case skipped and details are " + result.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result)
    {

    }

    @Override
    public void onStart(ITestContext result)
    {
    }

    @Override
    public void onFinish(ITestContext result)
    {
    }
}

